$(function() {
   
   $('#contact-submit').click(function(){
      event.preventDefault();
   
        if($('#name').val() == "") alert('Name can not be blank.');
        if($('#email').val() == "") alert('Email can not be blank.');
        if($('#comment').val() == "") alert('Comment can not be blank.');
        
        $.ajax({
            dataType: 'json',
            url: 'mailer.php',
            type:'post',
            data:{
                name: $('#name').val(),
                email: $('#email').val(),
                comment: $('#comment').val()
            },
            success: function(data) {
                if(data.status == "success") {
                    $('#message').addClass('alert-success').show(500).html(data.message).delay(5000).hide(500);
                    $("#form").trigger('reset');
                } 
                else {
                    $('#message').addClass('alert-danger').show(500).html(data.message).delay(5000).hide(500);
                }
            }
        })
    });

});