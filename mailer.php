<?php
if( isset($_POST) && !empty($_POST) ) {
    date_default_timezone_set('Etc/UTC');
    require 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
    
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Host = "mail.jswiftservices.com";
    $mail->setFrom('no-reply@jswiftservices.com', 'JSwift Services Contact Form');
    $mail->addAddress('info@jswiftservices.com', 'Jeffery Swift');
    $mail->addAddress('jffryswift@yahoo.com', 'Jeffery Swift');
    $mail->addAddress('tfletch55@gmail.com', 'Tony Fletcher');
    $mail->Subject = 'From JSwift Services Contact Form';
    
    $mail->msgHTML('
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
              <title>From JSwift Services Contact Form</title>
            </head>
            <body>
            <div style="width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;">
              <h1>From JSwift Services Contact Form</h1>
              <h3>Name:  '.$_POST['name'].'</h3>
              <h3>Email: '.$_POST['email'].'</h3>
              <h3>Comment: '.$_POST['comment'].'</h3>
            </div>
            </body>
        </html>    
    ');

    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    
    if($mail->send()) 
    {
        $response["status"] = "success";
        $response["message"] = "Your message has been sent!";
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response["message"] = 'Your message did not send - Please contact us at (615) 473-1396';
        echo json_encode($response);
    }
    
}
?>